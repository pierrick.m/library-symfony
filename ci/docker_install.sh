#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Update repository packages
apt-get update -yqq

# Install zip
apt-get install zip -yqq

# Install git (the php image doesn't have it) which is required by composer
apt-get install git -yqq

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install composer (Needed in order to resolve dependencies)
curl --silent --show-error https://getcomposer.org/installer | php

# Run composer to resolve dependencies
php composer.phar install

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-install pdo_mysql
