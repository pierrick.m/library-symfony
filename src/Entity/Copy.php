<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CopyRepository")
 */
class Copy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rented;

    /**
     * @ORM\Column(type="integer")
     */
    private $times_rented;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Book", inversedBy="copies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $book;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Rent", mappedBy="copy", cascade={"persist", "remove"})
     */
    private $rent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRented(): ?bool
    {
        return $this->rented;
    }

    public function setRented(bool $rented): self
    {
        $this->rented = $rented;

        return $this;
    }

    public function getTimesRented(): ?int
    {
        return $this->times_rented;
    }

    public function setTimesRented(int $times_rented): self
    {
        $this->times_rented = $times_rented;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getRent(): ?Rent
    {
        return $this->rent;
    }

    public function setRent(Rent $rent): self
    {
        $this->rent = $rent;

        // set the owning side of the relation if necessary
        if ($this !== $rent->getCopy()) {
            $rent->setCopy($this);
        }

        return $this;
    }
}
