<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author", inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Copy", mappedBy="book", orphanRemoval=true)
     */
    private $copies;

    /**
    * @ORM\Column(type="string")
    */
    private $title;

    /**
    * @ORM\Column(type="date")
    */
    private $publicationDate;

    public function __construct()
    {
        $this->copies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
      return $this->title;
    }

    public function setTitle(string $title): self
    {
      $this->title = $title;

      return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
      return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $date): self
    {
      $this->publicationDate = $date;

      return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Copy[]
     */
    public function getCopies(): Collection
    {
        return $this->copies;
    }

    public function addCopy(Copy $copy): self
    {
        if (!$this->copies->contains($copy)) {
            $this->copies[] = $copy;
            $copy->setBook($this);
        }

        return $this;
    }

    public function removeCopy(Copy $copy): self
    {
        if ($this->copies->contains($copy)) {
            $this->copies->removeElement($copy);
            // set the owning side to null (unless already changed)
            if ($copy->getBook() === $this) {
                $copy->setBook(null);
            }
        }

        return $this;
    }
}
