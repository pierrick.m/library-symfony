<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $first_name;

    /**
     * @ORM\Column(type="date")
     */
    private $date_joined;

    /**
     * @ORM\Column(type="integer")
     */
    private $books_rented;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Rent", mappedBy="client", cascade={"persist", "remove"})
     */
    private $rent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getDateJoined(): ?\DateTimeInterface
    {
        return $this->date_joined;
    }

    public function setDateJoined(\DateTimeInterface $date_joined): self
    {
        $this->date_joined = $date_joined;

        return $this;
    }

    public function getBooksRented(): ?int
    {
        return $this->books_rented;
    }

    public function setBooksRented(int $books_rented): self
    {
        $this->books_rented = $books_rented;

        return $this;
    }

    public function getRent(): ?Rent
    {
        return $this->rent;
    }

    public function setRent(Rent $rent): self
    {
        $this->rent = $rent;

        // set the owning side of the relation if necessary
        if ($this !== $rent->getClient()) {
            $rent->setClient($this);
        }

        return $this;
    }
}
