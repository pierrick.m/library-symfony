<?php

namespace App\Tests;

use App\Entity\Book;
use App\Entity\Author;
use App\Entity\Copy;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testAddingAuthor()
    {
      $author = new Author();
      $author->setFirstName('Aldous');
      $author->setLastName('Huxley');
      $author->setGender('M');

      $book = new Book();
      $book->setAuthor($author);

      $this->assertSame($book->getAuthor(), $author);
    }

    public function testAddingCopy()
    {
      $book = new Book();

      $copy = new Copy();
      $copy->setBook($book);

      $book->addCopy($copy);

      $this->assertTrue($book->getCopies()->contains($copy));
    }
}
