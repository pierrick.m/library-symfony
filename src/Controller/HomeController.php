<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;

class HomeController extends AbstractController
{
  /**
  * @Route("/")
  */
  public function displayHome(AuthorRepository $authorRepository, BookRepository $bookRepository): Response
  {
    return $this->render('home/index.html.twig', [
      'books' => $bookRepository->findAll()
    ]);
  }
}
